package org.marcpoppleton.simone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.debug.hv.ViewServer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;

import org.marcpoppleton.simone.bt.BluetoothChatService;
import org.marcpoppleton.simone.bt.DeviceListActivity;
import org.marcpoppleton.simone.ui.CatalogFragment;
import org.marcpoppleton.simone.ui.FriendsFragment;
import org.marcpoppleton.simone.ui.LogFragment;
import org.marcpoppleton.simone.ui.NewsFragment;
import org.marcpoppleton.simone.ui.PlayFragment;
import org.marcpoppleton.simone.ui.ProfilFragment;
import org.marcpoppleton.simone.ui.ResultFragment;
import org.marcpoppleton.simone.ui.dummy.DummyContent;
import org.marcpoppleton.simone.util.TypefaceSpan;


public class FullscreenActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener,ResultCallback<People.LoadPeopleResult>,CatalogFragment.IOnCatalogFragmentInteractionListener, PlayFragment.IOnPlayFragmentFragmentInteractionListener{

    /*
    Actualités
    Explorer
    Amis
    Mon profil
    Mon carnet de bord
*/
    private static final int SLIDER_MENU_PLAY = -1;
    private static final int SLIDER_MENU_NEWS = 0;
    private static final int SLIDER_MENU_EXPLORE = 1;
    private static final int SLIDER_MENU_FRIENDS = 2;
    private static final int SLIDER_MENU_PROFIL = 3;
    private static final int SLIDER_MENU_LOG = 4;

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private static final int REQUEST_RESOLVE_ERROR = 4;
    private static final String DIALOG_ERROR = "dialog_error";
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";


    private GoogleApiClient mGoogleApiClient;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mSliderMenuTitles;
    private boolean mResolvingError = false;

    private Person mCurrentPerson;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    private String mBluetoothDeviceAddress = "20:14:02:17:19:23";
    private int mCurrentBluetoothConnectionStatus = -1;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
        initBluetooth();
        setContentView(R.layout.activity_fullscreen);

        mTitle = mDrawerTitle = getTitle();
        mSliderMenuTitles = getResources().getStringArray(R.array.slidermenu_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mSliderMenuTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(false);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.actionbar_handle,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(SLIDER_MENU_NEWS);
        }
        ViewServer.get(this).addWindow(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
        // If BT is not on, request that it be enabled.
        // setupBluetooth() will then be called during onActivityResult
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            Log.d(PerrockApplication.PREFS_NAME,"onStart:initBluetooth");
            initBluetooth();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewServer.get(this).setFocusedWindow(this);
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        // Attempt to connect to the device
        final String bluetoothPacket = "C" + 0 + "#";
        byte[] send = bluetoothPacket.getBytes();
        PerrockApplication.getInstance().getChatService().write(send);
        PerrockApplication.getInstance().getChatService().disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ViewServer.get(this).removeWindow(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_RESOLVE_ERROR:
                mResolvingError = false;
                if (resultCode == RESULT_OK) {
                    // Make sure the app is not already connected or attempting to connect
                    if (!mGoogleApiClient.isConnecting() &&
                            !mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.connect();
                    }
                }
                break;
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(PerrockApplication.PREFS_NAME, "BT not enabled");
                    Toast.makeText(this, R.string.bluetooth_unavailable, Toast.LENGTH_LONG).show();
                }
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                showDefaultScreen();
                return true;
            case R.id.action_websearch:
                return true;
            case R.id.action_about:
                showAboutDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        SpannableString s = new SpannableString(mTitle);
        s.setSpan(new TypefaceSpan(this,"missionscript.otf"), 0, s.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        try {
            getActionBar().setTitle(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        // The good stuff goes here.
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            mCurrentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            Log.d(PerrockApplication.PREFS_NAME,"got current person data : " + mCurrentPerson.toString());
        }else{
            Log.d(PerrockApplication.PREFS_NAME,"Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) is null");
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
            Log.d(PerrockApplication.PREFS_NAME,"G+ connection suspended. Cause : " + cause);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API)
                        //.addApi(Drive.API)
                        //.addScope(Drive.SCOPE_FILE)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void initBluetooth() {
        // If BT is not on, request that it be enabled.
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (PerrockApplication.getInstance().getChatService() == null) setupChat();
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        PerrockApplication.getInstance().getChatService().connect(device, secure);
    }

    private void setupChat() {
        // Initialize the BluetoothChatService to perform bluetooth connections
        PerrockApplication.getInstance().setChatService(new BluetoothChatService(this, mHandler));
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment;
        Bundle args = new Bundle();
        switch (position) {
            case SLIDER_MENU_PROFIL:
                PerrockApplication.getInstance().track(getString(R.string.path_profile));
                fragment = new ProfilFragment();
                if (!(mCurrentPerson == null)) {
                    String personName = mCurrentPerson.getDisplayName();
                    String personPhotoUrl = mCurrentPerson.getImage().getUrl();
                    String personCoverUrl = mCurrentPerson.getCover().getCoverPhoto().getUrl();
                    args.putString(ProfilFragment.ARG_PARAM_NAME, personName);
                    args.putString(ProfilFragment.ARG_PARAM_PHOTO_URL, personPhotoUrl);
                    args.putString(ProfilFragment.ARG_PARAM_COVER_URL, personCoverUrl);
                    fragment.setArguments(args);
                }

                break;
            case SLIDER_MENU_LOG:
                PerrockApplication.getInstance().track(getString(R.string.path_logs));
                fragment = new LogFragment();
                break;
            case SLIDER_MENU_FRIENDS:
                PerrockApplication.getInstance().track(getString(R.string.path_logs));
                fragment = new FriendsFragment();
                break;
            case SLIDER_MENU_EXPLORE:
                PerrockApplication.getInstance().track(getString(R.string.path_catalog));
                fragment = new CatalogFragment();
                break;
            case SLIDER_MENU_NEWS:
                PerrockApplication.getInstance().track(getString(R.string.path_news));
            default:
                fragment = new NewsFragment();
        }

        setTitle(mSliderMenuTitles[position]);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    private void showDefaultScreen(){
        Fragment fragment;
        fragment = new NewsFragment();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    private void showAboutDialog() {
        AboutDialogFragment dialogFragment = new AboutDialogFragment();
        dialogFragment.show(getFragmentManager(), "aboutdialog");
    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                        case BluetoothChatService.STATE_CONNECTING:
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            mCurrentBluetoothConnectionStatus = msg.arg1;
                            break;
                        case MESSAGE_TOAST:
                            Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                                    Toast.LENGTH_SHORT).show();
                            break;
                    }
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                        // Get the message bytes and tell the BluetoothChatService to write
                        String message = "W1000#";
                        byte[] send = message.getBytes();
                        PerrockApplication.getInstance().getChatService().write(send);
                   Thread test = new Thread(new Runnable() {
                       @Override
                       public void run() {
                           try {
                               Thread.sleep(1000);
                           } catch (InterruptedException e) {
                               e.printStackTrace();
                           }
                           String message = "R255#C16711680#";
                        byte[] send = message.getBytes();
                        PerrockApplication.getInstance().getChatService().write(send);
                           try {
                               Thread.sleep(1000);
                           } catch (InterruptedException e) {
                               e.printStackTrace();
                           }
                           message = "C0#R0#";
                        send = message.getBytes();
                        PerrockApplication.getInstance().getChatService().write(send);

                       }
                   });
                    test.start();
                    break;
            }
        }
    };

    @Override
    public void onSelectedTrack(DummyContent.TrackItem trackItem) {
        PerrockApplication.getInstance().track(String.format(getString(R.string.path_catalog_selected), getResources().getString(trackItem.nameID)));
        Fragment fragment = new PlayFragment();
        Bundle args = new Bundle();
        args.putParcelable(PlayFragment.ARG_PARAM_TRACK, trackItem);
        fragment.setArguments(args);
        setTitle(getResources().getString(trackItem.nameID));
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void onGameFinished(DummyContent.TrackItem trackItem) {
        PerrockApplication.getInstance().track(String.format(getString(R.string.path_catalog_completed), getResources().getString(trackItem.nameID)));
        Fragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putParcelable(PlayFragment.ARG_PARAM_TRACK, trackItem);
        fragment.setArguments(args);
        setTitle(getResources().getString(trackItem.nameID));
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void onGamePaused(DummyContent.TrackItem trackItem, int position) {
        PerrockApplication.getInstance().track(String.format(getString(R.string.path_catalog_incompleted), getResources().getString(trackItem.nameID),position));
    }

    @Override
    public void onResult(People.LoadPeopleResult peopleData) {
        if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
            PersonBuffer personBuffer = peopleData.getPersonBuffer();
            try {
                int count = personBuffer.getCount();
                for (int i = 0; i < count; i++) {
                    Log.d(PerrockApplication.PREFS_NAME, "Display name: " + personBuffer.get(i).getDisplayName());
                }
            } finally {
                personBuffer.close();
            }
        } else {
            Log.e(PerrockApplication.PREFS_NAME, "Error requesting visible circles: " + peopleData.getStatus());
        }
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }



    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((FullscreenActivity) getActivity()).onDialogDismissed();
        }
    }

    public static class AboutDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.about)
                    .setPositiveButton(R.string.btn_about_dismiss, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dismiss();
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

}
