package org.marcpoppleton.simone;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.marcpoppleton.simone.bt.BluetoothChatService;

import java.util.HashMap;

/**
 * Created by marcpoppleton on 03/04/14.
 */
public class PerrockApplication extends Application {

    public static final String PREFS_NAME = "org.marcpoppleton.perrock";

    private static PerrockApplication mInstance;
    private static Context mAppContext;

    // Member object for the chat services
    private BluetoothChatService mChatService = null;


    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     *
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER // Tracker used only in this app.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        this.setAppContext(getApplicationContext());
    }

    public static PerrockApplication getInstance(){
        return mInstance;
    }
    public static Context getAppContext() {
        return mAppContext;
    }
    protected void setAppContext(Context mAppContext) {
        this.mAppContext = mAppContext;
    }
    protected void setChatService(BluetoothChatService chatService){this.mChatService = chatService;}
    public BluetoothChatService getChatService(){return mChatService;}

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(getString(R.string.ga_trackingId))
                    : analytics.newTracker(R.xml.global_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    public synchronized void track(String path){
        // Get tracker.
        Tracker t = getTracker(TrackerName.APP_TRACKER);

        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(path);

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }
}
