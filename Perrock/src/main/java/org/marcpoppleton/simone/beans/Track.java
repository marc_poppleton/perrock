package org.marcpoppleton.simone.beans;

import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by marcpoppleton on 15/04/14.
 */
public class Track {

    public static final int TRACK_BEGINNING = 0;
    public static final int TRACK_END = -1;

    private String mName;
    private String[] mFormattedNotes;
    private int mCurrentPosition = 0;

    private UUID mUUID;

    private static Pattern mPattern = Pattern.compile("([A-G][s]?)(\\d+)");

    private static final String[] notes = {"C", "Cs", "D", "Ds", "E", "F", "Fs", "G", "Gs", "A", "As", "B"};

    public Track(String name) {
        this.mUUID = UUID.randomUUID();
        this.mName = name;
        reset();
    }

    private Track(String name, String[] formattedNotes) {
        this.mUUID = UUID.randomUUID();
        this.mName = name;
        this.mFormattedNotes = formattedNotes;
        reset();
    }

    public static Track createTrack(String name, String[] formattedNotes) {
        return new Track(name, formattedNotes);
    }

    public UUID getmUUID() {
        return mUUID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getCurrentNoteIndex() {
        if(mCurrentPosition<mFormattedNotes.length) {
            return getNoteIndex(mCurrentPosition);
        }else{
            return -1;
        }
    }

    public void moveToNext() throws IndexOutOfBoundsException{
        mCurrentPosition++;
        if(mCurrentPosition>mFormattedNotes.length){
            throw new IndexOutOfBoundsException();
        }
    }

    public void moveToPrevious() throws IndexOutOfBoundsException{
        mCurrentPosition--;
        if(mCurrentPosition<0){
            throw new IndexOutOfBoundsException();
        }
    }

    public void moveToPosition(int position) throws IndexOutOfBoundsException{
        mCurrentPosition = position;
        if((mCurrentPosition<0)||(mCurrentPosition>mFormattedNotes.length)){
            throw new IndexOutOfBoundsException();
        }
    }

    public int getNoteIndex(int position) {
        return getNoteIndex(this.mFormattedNotes[position]);
    }

    public int getNotesCount() {
        return mFormattedNotes.length;
    }

    public void reset() {
        mCurrentPosition = TRACK_BEGINNING;
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public String[] getFormattedNotes() {
        return mFormattedNotes;
    }

    public boolean hasMoreNotes() {
        return mCurrentPosition < mFormattedNotes.length;
    }

    private static int getNoteIndex(String formattedNote) {
       /*
       Splitting the chain in two, first part is the note, second part is the scale
       IE : Cs4 => [Cs][4] Which reads C sharp in the 4th scale
        */
        Matcher matcher = mPattern.matcher(formattedNote);
        if (matcher.matches()) {
            return Arrays.asList(notes).indexOf(matcher.group(1)) + (Integer.parseInt(matcher.group(2)) * 12);

        } else {
            return -1;
        }
    }

    ;
}
