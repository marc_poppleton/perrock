package org.marcpoppleton.simone.ui;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.marcpoppleton.simone.R;
import org.marcpoppleton.simone.util.NoteHelper;

import java.util.ArrayList;
import java.util.List;

import be.hogent.tarsos.dsp.pitch.PitchDetectionResult;
import be.hogent.tarsos.dsp.pitch.Yin;


/**
 * TODO: document your custom view class.
 */
public class NoteDetectorView extends SurfaceView implements SurfaceHolder.Callback {


    private NoteDetectionThread mNoteDetectionThread;
    private boolean mDetectionPaused = false;
    private List<OnNoteDetectedListener> mOnNoteDetectedListener = new ArrayList<OnNoteDetectedListener>();

    public NoteDetectorView(Context context) {
        super(context);
        if(!isInEditMode()){
            createDetectionThread(context);
        }
    }

    public NoteDetectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()){
            createDetectionThread(context);
        }
    }

    public NoteDetectorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()){
            createDetectionThread(context);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // start the thread here so that we don't busy-wait in run()
        // waiting for the surface to be created
        mNoteDetectionThread.setRunning(true);
        if(!mNoteDetectionThread.isAlive()){
            mNoteDetectionThread.start();
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // we have to tell thread to shut down & wait for it to finish, or else
        // it might touch the Surface after we return and explode
        boolean retry = true;
        mNoteDetectionThread.setRunning(false);
        while (retry) {
            try {
                mNoteDetectionThread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void pauseDetection() {
        mDetectionPaused = true;

    }

    public void unPauseDetection() {
        mDetectionPaused = false;
    }

    public boolean isDetectionPaused() {
        return mDetectionPaused;
    }

    public void addOnNoteDetectedListener(OnNoteDetectedListener onNoteDetectedListener){
        mOnNoteDetectedListener.add(onNoteDetectedListener);
    }

    public void removeOnNoteDetectedListener(OnNoteDetectedListener onNoteDetectedListener){
        mOnNoteDetectedListener.remove(onNoteDetectedListener);
    }

    private void notifyListeners(NoteHelper.DetectedNote detectedNote){
        for (OnNoteDetectedListener listener : mOnNoteDetectedListener) {
            listener.onNoteDetected(detectedNote);
        }
    }


    private void createDetectionThread(Context context) {
        // register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        mNoteDetectionThread = new NoteDetectionThread(holder, context, new Handler() {
            @Override
            public void handleMessage(Message m) {
                switch (m.what){
                    case NoteDetectionThread.MSG_RESULT:
                        NoteHelper.DetectedNote detectedNote = m.getData().getParcelable("data");
                        notifyListeners(detectedNote);
                        break;
                    case NoteDetectionThread.MSG_ERROR:
                    default:
                        break;
                }
            }
        });
    }

    class NoteDetectionThread extends Thread{

        protected static final int MSG_ERROR = -1;
        protected static final int MSG_RESULT = 0;

        private boolean mRun = false;
        private SurfaceHolder mSurfaceHolder;
        private Context mContext;
        private Handler mHandler;

        private String[] mNotesNames;
        private TypedArray mNotesColors;

        AudioRecord recorder;
        int bufferSize = 0;
        int sampleRate = 44100;
        short audioFormat = AudioFormat.ENCODING_PCM_16BIT;
        short channelConfig = AudioFormat.CHANNEL_IN_MONO;
        int noteColor = getResources().getColor(R.color.bg_blue);

        public NoteDetectionThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
            mSurfaceHolder = surfaceHolder;
            mContext = context;
            mHandler = handler;

            mNotesNames = mContext.getResources().getStringArray(R.array.notes);
            mNotesColors = mContext.getResources().obtainTypedArray(R.array.notes_colors);
            try {
                bufferSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);
                recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, sampleRate, channelConfig, audioFormat, bufferSize);
            } catch (Exception e) {
                Message msg = mHandler.obtainMessage();
                Bundle b = new Bundle();
                b.putString("data", e.getLocalizedMessage());
                msg.what = MSG_ERROR;
                msg.setData(b);
                mHandler.sendMessage(msg);
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            Yin pitchDetector = new Yin(sampleRate, bufferSize);
            short[] audioData = new short[bufferSize];

            while (mRun) {
                if (recorder != null) {
                    if (isInterrupted())
                        break;
                    if (recorder.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
                        recorder.startRecording();
                    } else {
                        int numshorts = recorder.read(audioData, 0, audioData.length);
                        if ((numshorts != AudioRecord.ERROR_INVALID_OPERATION) && (numshorts != AudioRecord.ERROR_BAD_VALUE)) {
                            if (!isDetectionPaused()) {
                            updateSoundColor(pitchDetector.getPitch(floatMe(audioData)));
                            Canvas c = null;
                                try {
                                    c = mSurfaceHolder.lockCanvas(null);
                                    synchronized (mSurfaceHolder) {
                                        doDraw(c);
                                    }

                                } finally {
                                    // do this in a finally so that if an exception is thrown during the above, we don't leave the Surface in an inconsistent state
                                    if (c != null) {
                                        mSurfaceHolder.unlockCanvasAndPost(c);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            cleanup();
        }

        @Override
        public void interrupt() {
            super.interrupt();
            cleanup();
        }

        /**
         * Used to signal the thread whether it should be running or not.
         * Passing true allows the thread to run; passing false will shut it
         * down if it's already running. Calling start() after this was most
         * recently called with false will result in an immediate shutdown.
         *
         * @param b true to run, false to shut down
         */
        public void setRunning(boolean b) {
            mRun = b;
        }

        private void updateSoundColor(PitchDetectionResult pitchDetectionResult){
            if (pitchDetectionResult.getPitch() != -1) {
                float pitch = pitchDetectionResult.getPitch();
                float probability = pitchDetectionResult.getProbability();
                NoteHelper.DetectedNote closestDetectionResult = NoteHelper.closestNote(pitch);
                final int closestNoteIndex = closestDetectionResult.getClosestPureNoteIndex();
                // if the distance is negative, then the closest note is below the detected pitch
                // if the distance is nil, then the pitch is dead on a note in the tempered scale
                // if the distance is positive, then the closest note is above the detected pitch
                float closestNoteDistance = closestDetectionResult.getDetectionDistance();

                ArgbEvaluator argbEvaluator = new ArgbEvaluator();
                if (closestNoteDistance < 0) {
                    float closestLowerNoteFrequency = NoteHelper.frequencies[closestNoteIndex];
                    float closestUpperNoteFrequency = NoteHelper.frequencies[closestNoteIndex + 1];
                    float fraction = Math.abs(closestNoteDistance) / (closestUpperNoteFrequency - closestLowerNoteFrequency);
                    int closestLowerNoteColor = mNotesColors.getColor(closestNoteIndex % mNotesNames.length, 0);
                    int closestUpperNoteColor = mNotesColors.getColor((closestNoteIndex + 1) % mNotesNames.length, 0);
                    noteColor = (Integer) argbEvaluator.evaluate(fraction, closestLowerNoteColor, closestUpperNoteColor);
                }
                if (closestNoteDistance > 0) {
                    float closestLowerNoteFrequency = NoteHelper.frequencies[closestNoteIndex - 1];
                    float closestUpperNoteFrequency = NoteHelper.frequencies[closestNoteIndex];
                    float fraction = Math.abs(closestNoteDistance) / (closestUpperNoteFrequency - closestLowerNoteFrequency);

                    int closestLowerNoteColor = mNotesColors.getColor((closestNoteIndex - 1) % mNotesNames.length, 0);
                    int closestUpperNoteColor = mNotesColors.getColor(closestNoteIndex % mNotesNames.length, 0);
                    noteColor = (Integer) argbEvaluator.evaluate(fraction, closestLowerNoteColor, closestUpperNoteColor);
                }
                if (-1 < closestNoteDistance && closestNoteDistance < 1) {
                    noteColor = mNotesColors.getColor(closestNoteIndex % mNotesNames.length, 0);
                }

                closestDetectionResult.setDetectedNoteColor(noteColor);
                notifyObservers(closestDetectionResult);
            }
        }

        protected void doDraw(Canvas c) {
            if(mRun){
                c.drawColor(noteColor);
            }
        }

        private void notifyObservers(NoteHelper.DetectedNote detectedNote){
            Message msg = mHandler.obtainMessage();
            Bundle b = new Bundle();
            b.putParcelable("data", detectedNote);
            msg.what = MSG_RESULT;
            msg.setData(b);
            mHandler.sendMessage(msg);
        }

        private void cleanup() {
            mNotesColors.recycle();
            if (recorder != null) {
                if (recorder.getState() == AudioRecord.RECORDSTATE_RECORDING)
                    try {
                        recorder.stop(); //stop the recorder before ending the thread
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                recorder.release();
                recorder = null;
            }
        }

        private float[] floatMe(short[] pcms) {
            float[] floaters = new float[pcms.length];
            for (int i = 0; i < pcms.length; i++) {
                floaters[i] = pcms[i];
            }
            return floaters;
        }
    }

    public interface OnNoteDetectedListener{
        public void onNoteDetected(NoteHelper.DetectedNote result);
    }
}

