package org.marcpoppleton.simone.ui;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.marcpoppleton.simone.PerrockApplication;
import org.marcpoppleton.simone.R;
import org.marcpoppleton.simone.beans.Track;
import org.marcpoppleton.simone.bt.BluetoothChatService;
import org.marcpoppleton.simone.ui.dummy.DummyContent;
import org.marcpoppleton.simone.util.NoteHelper;

import java.io.IOException;
import java.util.ArrayList;

public class PlayFragment extends Fragment {

    public static final String ARG_PARAM_TRACK = "param_track";
    private static final int MAX_PLAYBACK_TIME = 1100;
    private static final int DEFAULT_PLAYBACK_TIME = 500;
    private static final int MIN_PLAYBACK_TIME = 150;

    private View mView;
    private NoteDetectorView mDetectionView;
    private TextView mNoteName;
    private ProgressBar mProgress;
    private ToggleButton mGamePause;
    private ImageButton mGameReload, mTempoSlow, mTempoFast;
    private Track mTrack;
    private String[] mNotesNames;
    private TypedArray mNotesColors;
    private SoundPool mSoundPool;
    final ArrayList<Integer> mSoundIDs = new ArrayList<Integer>();
    private int mPlaybackTime = 500;
    private AsyncTask mCurrentTask;
    private DummyContent.TrackItem mTrackRef;
    private IOnPlayFragmentFragmentInteractionListener mListener;

    public static PlayFragment newInstance(String param1, String param2) {
        PlayFragment fragment = new PlayFragment();
        return fragment;
    }

    public PlayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTrackRef = getArguments().getParcelable(ARG_PARAM_TRACK);
            if (mTrackRef != null) {
                mTrack = Track.createTrack(getResources().getString(mTrackRef.nameID), getResources().getStringArray(mTrackRef.notesArrayID));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Typeface myTypeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/missionscript.otf");
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_play, container, false);
        mDetectionView = (NoteDetectorView) mView.findViewById(R.id.note_detection);
        mNotesNames = getResources().getStringArray(R.array.notes);
        mNotesColors = getResources().obtainTypedArray(R.array.notes_colors);
        mProgress = (ProgressBar) mView.findViewById(R.id.progress_game);
        mNoteName = (TextView) mView.findViewById(R.id.note_name);
        mNoteName.setTypeface(myTypeface);
        mTempoSlow = (ImageButton) mView.findViewById(R.id.btn_tempo_slow);
        mTempoSlow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int newTime = (int) (mPlaybackTime * 1.25);
                if (newTime > MAX_PLAYBACK_TIME) {
                    mPlaybackTime = newTime;
                }
                {
                    mPlaybackTime = MAX_PLAYBACK_TIME;
                }
            }
        });
        mTempoFast = (ImageButton) mView.findViewById(R.id.btn_tempo_fast);
        mTempoFast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int newTime = (int) (mPlaybackTime * 0.75);
                if (newTime < MIN_PLAYBACK_TIME) {
                    mPlaybackTime = newTime;
                }
                {
                    mPlaybackTime = MIN_PLAYBACK_TIME;
                }
            }
        });
        mGameReload = (ImageButton) mView.findViewById(R.id.btn_game_reload);
        mGameReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTrack.reset();
                mDetectionView.pauseDetection();
                new PlayPreview().execute(mSoundIDs);
            }
        });
        mGamePause = (ToggleButton) mView.findViewById(R.id.btn_game_pause);
        mGamePause.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDetectionView.unPauseDetection();
                } else {
                    mDetectionView.pauseDetection();
                }
            }
        });
        mDetectionView.pauseDetection();
        mDetectionView.addOnNoteDetectedListener(new NoteDetectorView.OnNoteDetectedListener() {
            @Override
            public void onNoteDetected(NoteHelper.DetectedNote result) {
                if (result != null) {
                    sendRecognisedColor(result.getDetectedNoteColor());
                    if (mTrack.hasMoreNotes()) {
                        final float expectedNoteFrequency = NoteHelper.frequencies[mTrack.getCurrentNoteIndex()];
                        final float maxAllowedistance = expectedNoteFrequency * 0.10f;
                        float relativeDistance = (result.getDetectedNote() - expectedNoteFrequency) + (maxAllowedistance / 2);
                        final int maxAllowedistanceRnd = Math.round(maxAllowedistance);

                        final String closestNoteName = mNotesNames[result.getClosestPureNoteIndex() % mNotesNames.length];
                        final String expectedNoteName = mNotesNames[mTrack.getCurrentNoteIndex() % mNotesNames.length];
                        if (expectedNoteName.equals(closestNoteName)) {
                            Log.d(PerrockApplication.PREFS_NAME, "detected note (" + closestNoteName + ") is expected note: " + expectedNoteName);
                            mTrack.moveToNext();
                            updateUI();

                        }
                    } else {
                        if (null != mListener) {
                            new SuccessAnimationTask().execute();
                            mListener.onGameFinished(mTrackRef);
                        }
                    }
                }
            }
        });
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mProgress.setMax(mTrack.getNotesCount() - 1);
        mProgress.setProgress(mTrack.getCurrentPosition());
        if (mTrack.getCurrentPosition() == Track.TRACK_BEGINNING) {
            final ArrayList<Integer> loadedmSoundIDs = new ArrayList<Integer>();
            // Load the sound
            mSoundPool = new SoundPool(mTrack.getNotesCount(), AudioManager.STREAM_MUSIC, 0);
            mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    loadedmSoundIDs.add(sampleId);
                    if (mSoundIDs.size() == loadedmSoundIDs.size()) {
                        mCurrentTask = new PlayPreview().execute(mSoundIDs);

                    }

                }
            });
            try {
                String[] notes = mTrack.getFormattedNotes();
                for (String note : notes) {
                    final String filename = "sounds/" + note + ".wav";
                    mSoundIDs.add(mSoundPool.load(getResources().getAssets().openFd(filename), 1));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mCurrentTask != null)
            mCurrentTask.cancel(true);
        mSoundPool.release();
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onGamePaused(mTrackRef, mTrack.getCurrentPosition());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(PerrockApplication.PREFS_NAME, "onAttach");
        try {
            mListener = (IOnPlayFragmentFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private class PlayPreview extends AsyncTask<ArrayList<Integer>, Void, Void> {

        @Override
        protected Void doInBackground(ArrayList<Integer>... params) {
            final ArrayList<Integer> soundIDs = new ArrayList<Integer>(params[0]);
            while (soundIDs.size() > 0) {
                final int soundId = soundIDs.remove(0);
                sendExpectedColor(mNotesColors.getColor(mTrack.getCurrentNoteIndex() % mNotesNames.length, 0));
                mSoundPool.play(soundId, 1, 1, 1, 0, 1);
                publishProgress();
                try {
                    Thread.sleep(mPlaybackTime);
                    mSoundPool.stop(soundId);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            updateUI();
            mTrack.moveToNext();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mTrack.reset();
            updateUI();
            mDetectionView.unPauseDetection();
        }
    }

    private class SuccessAnimationTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            for (int flashes = 0; flashes < 2; flashes++) {
                sendColor(0);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sendColor(0xFFF);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sendColor(0);
            }
            return null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void updateUI() {
        if (mTrack.hasMoreNotes()) {
            mNoteName.setText(mNotesNames[mTrack.getCurrentNoteIndex() % mNotesNames.length]);
            mView.setBackgroundColor(mNotesColors.getColor(mTrack.getCurrentNoteIndex() % mNotesNames.length, R.color.cream_light));
            mProgress.setProgress(mTrack.getCurrentPosition());
            sendExpectedColor(mNotesColors.getColor(mTrack.getCurrentNoteIndex() % mNotesNames.length, 0));
        } else {
            sendExpectedColor(0);
        }

    }

    private void sendRecognisedColor(int color) {
        final String bluetoothPacket = "R" + color + "#";
        sendMessage(bluetoothPacket);
    }

    private void sendExpectedColor(int color) {
        final String bluetoothPacket = "C" + color + "#";
        sendMessage(bluetoothPacket);
    }

    private void sendColor(int color) {
        final String bluetoothPacket = "R" + color + "#C" + color + "#";
        sendMessage(bluetoothPacket);
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (PerrockApplication.getInstance().getChatService().getState() == BluetoothChatService.STATE_CONNECTED) {
            // Check that there's actually something to send
            if (message.length() > 0) {
                // Get the message bytes and tell the BluetoothChatService to write
                byte[] send = message.getBytes();
                PerrockApplication.getInstance().getChatService().write(send);
            }
        }
    }

    public interface IOnPlayFragmentFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onGameFinished(DummyContent.TrackItem id);

        public void onGamePaused(DummyContent.TrackItem id, int position);
    }


}
