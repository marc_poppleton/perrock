package org.marcpoppleton.simone.ui;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.marcpoppleton.simone.PerrockApplication;
import org.marcpoppleton.simone.R;
import org.marcpoppleton.simone.util.VolleySingleton;

import java.text.DateFormat;
import java.util.Date;

public class ProfilFragment extends Fragment {
    public static final String ARG_PARAM_NAME = "param_name";
    public static final String ARG_PARAM_PHOTO_URL = "param_photo_url";
    public static final String ARG_PARAM_COVER_URL = "param_cover_url";

    private String mParamName;
    private String mParamPhotoUrl;
    private String mParamCoverUrl;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfilFragment.
     */
    public static ProfilFragment newInstance(String param1, String param2, String param3) {
        ProfilFragment fragment = new ProfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_NAME, param1);
        args.putString(ARG_PARAM_PHOTO_URL, param2);
        args.putString(ARG_PARAM_COVER_URL, param3);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfilFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamName = getArguments().getString(ARG_PARAM_NAME);
            mParamPhotoUrl = getArguments().getString(ARG_PARAM_PHOTO_URL);
            mParamCoverUrl = getArguments().getString(ARG_PARAM_COVER_URL);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profil, container, false);
        ((TextView) view.findViewById(R.id.profile_name)).setText(mParamName);
        ImageLoader mImageLoader = VolleySingleton.getInstance().getImageLoader();

        ((NetworkImageView) view.findViewById(R.id.profile_image)).setImageUrl(mParamPhotoUrl, mImageLoader);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (menu != null) {
            MenuItem item = menu.findItem(R.id.action_websearch);
            if (item != null) {
                item.setVisible(false);
            }
        }
    }

    private String getFirstLaunchDate() {
        SharedPreferences settings = getActivity().getSharedPreferences(PerrockApplication.PREFS_NAME, 0);
        String firstLaunchDate = settings.getString("firstLaunch", null);
        if (firstLaunchDate == null) {
            SharedPreferences.Editor editor = settings.edit();
            firstLaunchDate = DateFormat.getDateInstance().format(new Date());
            editor.putString("firstLaunch", firstLaunchDate);
            editor.commit();
        }
        Resources res = getResources();
        return String.format(res.getString(R.string.user_since), firstLaunchDate);
    }

}
