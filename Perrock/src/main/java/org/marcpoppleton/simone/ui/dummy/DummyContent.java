package org.marcpoppleton.simone.ui.dummy;

import android.os.Parcel;
import android.os.Parcelable;

import org.marcpoppleton.simone.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<TrackItem> ITEMS = new ArrayList<TrackItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<Integer, TrackItem> ITEM_MAP = new HashMap<Integer, TrackItem>();

    static {
        addItem(new TrackItem(R.string.major_scale_4,R.string.scales,R.array.major_scale_4,R.drawable.cover_scales));
        addItem(new TrackItem(R.string.minor_scale_4,R.string.scales,R.array.minor_scale_4,R.drawable.cover_scales));
        addItem(new TrackItem(R.string.chromatic_scale_4,R.string.scales,R.array.chromatic_scale_4,R.drawable.cover_scales));
        addItem(new TrackItem(R.string.pop_corn,R.string.hot_butter,R.array.pop_corn,R.drawable.cover_popcorn));
        addItem(new TrackItem(R.string.stairway,R.string.led_zep,-1,R.drawable.cover_stairway));
        addItem(new TrackItem(R.string.the_whistleblowers,R.string.laibach,R.array.the_whistleblowers,R.drawable.cover_the_whistleblowers));
        addItem(new TrackItem(R.string.fur_elise,R.string.beethoven,R.array.fur_elise,R.drawable.cover_fur_elise));
    }

    private static void addItem(TrackItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.nameID, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class TrackItem implements Parcelable{
        public int nameID;
        public int artistID;
        public int notesArrayID;
        public int coverID;

        public TrackItem(int id, int artist, int content,int cover) {
            nameID = id;
            artistID = artist;
            notesArrayID = content;
            coverID = cover;
        }

        public TrackItem(Parcel in){
            nameID = in.readInt();
            artistID = in.readInt();
            notesArrayID = in.readInt();
            coverID = in.readInt();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(nameID);
            dest.writeInt(artistID);
            dest.writeInt(notesArrayID);
            dest.writeInt(coverID);

        }

        public static final Parcelable.Creator<TrackItem> CREATOR = new Parcelable.Creator<TrackItem>() {
            public TrackItem createFromParcel(Parcel in) {
                return new TrackItem(in);
            }

            public TrackItem[] newArray(int size) {
                return new TrackItem[size];
            }
        };
    }
}
