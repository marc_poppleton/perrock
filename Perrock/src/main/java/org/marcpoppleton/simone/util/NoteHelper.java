package org.marcpoppleton.simone.util;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Created by marcpoppleton on 21/02/14.
 */
public class NoteHelper {

    //Tempered scale frequencies covering 8 octaves
    public static float[] frequencies = {
            //C 0       C# 1        D 2         D# 3        E 4         F 5         F# 6        G 7         G# 8        A 9         A# 10       B 11
            //Do 0      Do# 1       Ré 2        Mi♭ 3       Mi 4        Fa 5        Fa# 6       Sol 7       La♭ 8       La 9        Si♭ 10      Si 11
            32.70f,     34.65f,     36.71f,     38.89f,     41.20f,     43.65f,     46.25f,     49.00f,     51.91f,     55.00f,     58.27f,     61.74f,
            65.41f,     69.30f,     73.42f,     77.78f,     82.41f,     87.31f,     92.50f,     98.00f,     103.83f,    110.00f,    116.54f,    123.47f,
            130.81f,    138.59f,    146.83f,    155.56f,    164.81f,    174.61f,    185.00f,    196.00f,    207.65f,    220.00f,    233.08f,    246.94f,
            261.63f,    277.18f,    293.66f,    311.13f,    329.63f,    349.23f,    369.99f,    392.00f,    415.30f,    440.00f,    466.16f,    493.88f,
            523.25f,    554.37f,    587.33f,    622.25f,    659.26f,    698.46f,    739.99f,    783.99f,    830.61f,    880.00f,    932.33f,    987.77f,
            1046.50f,   1108.73f,   1174.66f,   1244.51f,   1318.51f,   1396.91f,   1479.98f,   1567.98f,   1661.22f,   1760.00f,   1864.66f,   1975.53f,
            2093.00f,   2217.46f,   2349.32f,   2489.02f,   2637.02f,   2793.83f,   2959.96f,   3135.96f,   3322.44f,   3520.00f,   3729.31f,   3951.07f,
            4186.01f,   4434.92f,   4698.64f,   4978.03f,   5274.04f,   5587.65f,   5919.91f,   6271.93f,   6644.88f,   7040.00f,   7458.62f,   7902.13f
    };

    public static final int notesInScale = 12;

    /**
     * Returns the closest tempered scale note frequency for a given pitch frequency.
     * The method also returns the distance between the given pitch frequency and the returned note frequency.
     * @param pitch the frequency of the pitch
     * @return a Pair of Float containing the closest tempered scale note index and the distance in Htz from the note.
     */
    public static final DetectedNote closestNote(final float pitch) {

        Float distance = Float.valueOf(0f);
        int n = Arrays.binarySearch(frequencies, pitch);
        if (n < 0) {
            // when n is an insertion point
            n = (-n) - 1; // Convert n to an index
            // If n == 0 we don't need to find nearest neighbor. If n is
            // larger than the array. use the last point in the array.
            if (n > frequencies.length - 1) {
                n = frequencies.length - 1; // If key is larger than any value in values
            } else if (n > 0) {
                // find nearest neighbor
                float d1 = frequencies[n - 1] - pitch;
                float d2 = frequencies[n] - pitch;
                if (Math.abs(d1) <= Math.abs(d2)) {
                    n = n - 1;
                    distance = Float.valueOf(d1);
                } else {
                    distance = Float.valueOf(d2);
                }
            }
        }
        return new DetectedNote(pitch, n, distance);
    }

    public static class DetectedNote implements Parcelable{
        private int mClosestPureNoteIndex;
        private float mDetectionDistance;
        private float mDetectedNote;
        private int mDetectedNoteColor;

        public DetectedNote(){}

        protected DetectedNote(float detectedNote, int closestPureNoteIndex,float detectionDistance){
            this.mDetectedNote = detectedNote;
            this.mClosestPureNoteIndex = closestPureNoteIndex;
            this.mDetectionDistance = detectionDistance;
        }
        protected DetectedNote(float detectedNote, int closestPureNoteIndex,int detectedNoteColor, float detectionDistance){
            this.mDetectedNote = detectedNote;
            this.mClosestPureNoteIndex = closestPureNoteIndex;
            this.mDetectedNoteColor = detectedNoteColor;
            this.mDetectionDistance = detectionDistance;
        }

        @Override
        public String toString() {
            return "closestNoteIndex(" + mClosestPureNoteIndex + "), detectionDistance(" + mDetectionDistance + ")";
        }

        public int getClosestPureNoteIndex(){
            return mClosestPureNoteIndex;
        }
        public int getDetectedNoteColor(){
            return mDetectedNoteColor;
        }
        public float getDetectedNote(){
            return mDetectedNote;
        }
        public void setDetectedNoteColor(int detectedNoteColor){
            this.mDetectedNoteColor = detectedNoteColor;
        }
        public float getDetectionDistance(){
            return mDetectionDistance;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(mClosestPureNoteIndex);
            dest.writeInt(mDetectedNoteColor);
            dest.writeFloat(mDetectionDistance);
        }

        public static final Creator<DetectedNote> CREATOR = new Creator<DetectedNote>() {
            @Override
            public DetectedNote createFromParcel(Parcel source) {
                DetectedNote detectedNote = new DetectedNote();
                detectedNote.mClosestPureNoteIndex = source.readInt();
                detectedNote.mDetectedNoteColor = source.readInt();
                detectedNote.mDetectionDistance = source.readFloat();
                return detectedNote;
            }

            @Override
            public DetectedNote[] newArray(int size) {
                return new DetectedNote[size];
            }
        };
    }

}
