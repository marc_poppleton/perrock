# Perrock #

## Changelog ##

### 1.0.5 (2014-06-05)


#### Bug Fixes

* generated G+ API tokens for the app package name
* tempo increases and decreases by 25% increments


#### Features

* custom design in the action bar (font + nav icons)
* Mission script font dans l'action bar
* layout with covers in the catalog


### 1.0.4 (2014-06-03)


#### Features

* Stairway To Heaven - Led Zeppelin song in the catalog
* Laibach - The Whistleblowers song in the catalog
* Pop Corn song in the catalog


### 1.0.3 (2014-06-31)


#### Bug Fixes

* expected color is now displayed until note is found
* expected color is now displayed until note is found


### 1.0.2 (2014-05-28)


#### Bug Fixes

* screen orientation blocked to portrait
* minor bug fixes, Lint related


#### Features

* release Beta 1.0.2 published on Store
* dummy screens
* selection of scales to play along
* track played on piano before player can play
* 3 full scales piano sample
* started to implement the ui design for the play sequences
* adding custom font
* added UI specs
* added BT connectivity with Perrock device
* added Google Play Services API
* added OnNoteDetectedListener interface so that other components can be notified of note detections.
* added navigation items (slider menu and action bar)


### 1.0.1 (2014-02-24)


#### Bug Fixes

* we always returned the lower bound closest frequency, never the upper bound closest freq.


#### Features

* added color interpolation for when the pitch is not dead on a note in the scale
* added Crashlytics support to the app


### 1.0 (2014-02-23)


#### Bug Fixes

* unable to run detection after a second run of the app. The task was not cancelling


#### Features

* Added background color changing following the note colors scale
* First import of project code.